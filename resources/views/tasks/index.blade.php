@extends('layouts.app')

@section('content')

<h1>This is your task list</h1>
<a href="{{route('mytasks')}}">My tasks </a>
<ul>
    @foreach($tasks as $task)
    <li>
        <!-- > id: {{$task->id}} title:{{$task->title}} <!-->
       @if ($task->status)
           <input type = 'checkbox' id ="{{$task->id}}" checked>
      @else
           <input type = 'checkbox' id ="{{$task->id}}">
       @endif
     
     {{$task->title}} 
       <a href= "{{route('tasks.edit', $task->id )}}"> Edit  </a>
       @cannot('employee')<a href= "{{route('delete', $task->id )}}"> Delete  </a>@endcannot
       
       
       @if ($task->status!=1)
       @cannot('employee') <a href= "{{route('done', $task->id )}}"> Mark as done  </a> @endcannot
       @else
       <a>done</a>
       @endif
      

    </li>
    
   
    @endforeach
</ul>

<a href="{{route('tasks.create')}}">Create New task </a>

<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url: "{{url('tasks')}}" + '/' + event.target.id ,
                   dataType:'json' ,
                   type:'put',
                   contentType:'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
            });
       });
</script>  

@endsection
