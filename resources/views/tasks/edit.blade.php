@extends('layouts.app')
@section('content')

<h1>Edit task</h1>
<form method = 'post' action="{{action('TaskController@update', $task->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Task to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$task->title}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="save changes">
</div>

</form>
<form method = 'post' action="{{action('TaskController@destroy', $task->id)}}">
@csrf
@method('DELETE')
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete task">
</div>

</form>
@endsection
