<?php

namespace App\Http\Controllers;
use App\User;

use Illuminate\Http\Request;
use App\Task; 
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$id=Auth::id(); 
        $tasks = Task::all();
        //$tasks = User::find($id)->tasks;
        return view('tasks.index', ['tasks'=>$tasks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        return view ('tasks.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task();
        $id=Auth::id();
        $task->title = $request->title;
        $task->user_id = $id;
        $task->save();
        return redirect('tasks');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit', compact('task'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        if(!$task->user->id == Auth::id()) return(redirect('tasks'));
        $task -> update($request->except(['_token']));
        if($request->ajax()){
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }
        return redirect('tasks');

        //$todo -> update($request->all());
       // return redirect('todos');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to create tasks..");
        } 

        $task = Task::find($id);
        $task->delete();
        return redirect('tasks');

    }

    public function done($id)
    {

        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to create tasks..");
        } 
        $task = Task::find($id);
        $task->status = 1;
        $task->save();
        $task->update();
        return redirect('tasks');

    }
    public function mytasks()
    {
        $id=Auth::id(); 
        $user = User::find($id);
        $tasks = $user->tasks;
        //$task = Task::find($id);
        //$tasks = User::find($id)->tasks;
        return view('tasks.mytasks', ['tasks'=>$tasks]);
    }
   



}
