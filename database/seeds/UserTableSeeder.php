<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'jack',
                        'email' => 'a@a.com',
                        'password' => Hash::make('12345678'),
                        'role' => 'admin',
                        'created_at' => date('Y-m-d G:i:s'),
                ]
            
                    ]);
    }

}
